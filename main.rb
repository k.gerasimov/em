#!/usr/bin/env ruby
# frozen_string_literal: true

require 'rubygems'
require 'eventmachine'
require 'em-http-request'
require 'fiber'
require 'json'

@_initiated = 0
@_completed = 0
@options = {
    connect_timeout: 1,
    inactivity_timeout: 10,
    ssl: {
        verify_peer: false
    }
}

def print_msg
  period = (@ended_at - @started_at).to_i
  rps = @_completed / period if period.positive?
  print [
    "\rInitiated: #{@_initiated}/#{@max_count}, Success: #{@_completed}, Filed: #{@_filed}, Time (sec): #{(@ended_at - @started_at).to_i}",
    "requests per seconds (RPS): #{rps}"
].join(", ")
end

def stop(timer)
  EM.cancel_timer(timer) unless timer.nil?
  EM.stop
end

brand_id = 'e14dbf7d-2a25-4def-aa5b-06098072e42c'
@url = "https://auth-platform.evosoft.xyz/api/v1/protected/auth/get_link?brand_id=#{brand_id}&kind=telegram"
# @request_options = {
#   keepalive: true,
#   head: {
#     Accept: '*/*',
#     'Content-Type': 'application/graphql',
#     'x-api-key': 'da2-lhntlyk5ijborh5ayfcbrlymvm'
#   },
#   body: '{ "query": "query { allBlocks {id worker_full_name} }" }'
# }
@request_options = {
  keepalive: true,
  head: {
    Accept: '*/*',
    'Content-Type': 'application/json; charset=utf-8', # 'text/html', 'application/graphql',
    'Cache-Control': 'no-cache, must-revalidate',
    'Authorization': ''
    # 'Cookie': '_ym_uid=1575895603142832781; _ym_d=1575895603; _ga=GA1.2.1936267157.1575919838; request_method=GET; _production_science_list_session=eFhOLy9iczJkUkN4NlhVYStLU3cyRjlYend6M2Jma2tqcXh4TEgxaVdiN1g3UStVSG5BU1VvMEE4K1ptK2dKM2dPZ3hHZVpKQUxVRS9Wc24wckJQWWh6VFZmZzJkWjNjbGlPU1dDSUNIZzVzWXNCV0dmbytXMjAyeEhIRThWVER2RVdnbnFhdFVmUjd6L2tTQVY4Z3FDN0R4QWdBTFZoK0ZzSnZ5MTlEa0wvVjloT3FkRG4wa0hLMGhDL0cwT1RBKzl4Rm9EOVdsdmROMWlJSC9STHdGb3A1dHdSS1lBdXFBZ1MreUsyT2h1cmlpQVBBc3AxVEkrWmZ1RFlQRGczTFhGSmNFdEZ3VklDeHYzT01tM3VhTkZxNm9wVVRBZmxhTEtPRExRWGYwOW5wKzFVUUsrTlp2cFpuYmRVeTFOZjJaT1NYelhKZkpzR3pNaU1seG9vZDA0MlhCOHNZWXg2ZUF1ZElnY0ZNeW1uaktwZ2lPK1h0azV2bkVIMGlOYW9jZmNkM1dPZ2xWaFZjV2ZJK1dpSU50bjBlaGtDU0Zld1cvUHd1NUtkcml6Yz0tLUMvYVZsSWlyZVFxWVRZUENEWDQ4N0E9PQ%3D%3D--4280b550e21377816556d7d2c4a2d8b696dc9a3a'
  },
  body: ''
}
@post_url = "https://auth-platform.evosoft.xyz/api/v1/protected/auth/create_session"
@post_request_options = {
  keepalive: true,
  head: {
    Accept: '*/*',
    'Content-Type': 'application/json; charset=utf-8', # 'text/html', 'application/graphql',
    'Cache-Control': 'no-cache, must-revalidate',
    'Authorization': ''
    # 'Cookie': '_ym_uid=1575895603142832781; _ym_d=1575895603; _ga=GA1.2.1936267157.1575919838; request_method=GET; _production_science_list_session=eFhOLy9iczJkUkN4NlhVYStLU3cyRjlYend6M2Jma2tqcXh4TEgxaVdiN1g3UStVSG5BU1VvMEE4K1ptK2dKM2dPZ3hHZVpKQUxVRS9Wc24wckJQWWh6VFZmZzJkWjNjbGlPU1dDSUNIZzVzWXNCV0dmbytXMjAyeEhIRThWVER2RVdnbnFhdFVmUjd6L2tTQVY4Z3FDN0R4QWdBTFZoK0ZzSnZ5MTlEa0wvVjloT3FkRG4wa0hLMGhDL0cwT1RBKzl4Rm9EOVdsdmROMWlJSC9STHdGb3A1dHdSS1lBdXFBZ1MreUsyT2h1cmlpQVBBc3AxVEkrWmZ1RFlQRGczTFhGSmNFdEZ3VklDeHYzT01tM3VhTkZxNm9wVVRBZmxhTEtPRExRWGYwOW5wKzFVUUsrTlp2cFpuYmRVeTFOZjJaT1NYelhKZkpzR3pNaU1seG9vZDA0MlhCOHNZWXg2ZUF1ZElnY0ZNeW1uaktwZ2lPK1h0azV2bkVIMGlOYW9jZmNkM1dPZ2xWaFZjV2ZJK1dpSU50bjBlaGtDU0Zld1cvUHd1NUtkcml6Yz0tLUMvYVZsSWlyZVFxWVRZUENEWDQ4N0E9PQ%3D%3D--4280b550e21377816556d7d2c4a2d8b696dc9a3a'
  },
  body: {
    external_id: 'e14dbf7d-2a25-4def-aa5b-06098072e400',
    bot_id: '0811b695-3fe4-4b86-82c1-214dc92b5877',
    language: 'rus'
}.to_json
}

@max_count = 3000 # Максимальное количество звпросов
period = 0.001 # Время между запросами (в секундах). Отправлять каждые n секунд.

@_filed = 0
@until_the_end = nil

@_errors = []

@started_at = Time.now.to_i
@ended_at = Time.now.to_i
@initiated_at = Time.now.to_i
@requests = {}; @completed = {}

print_msg

EventMachine.run {
  @until_the_end = @max_count

  timer = EventMachine.add_periodic_timer(period) {
    request = EM::HttpRequest.new(@post_url, @options).post @post_request_options
    # request = EM::HttpRequest.new(@url, @options).get @request_options

    @_initiated += 1

    request.callback { |http|
      # https://www.rubydoc.info/gems/em-http-request/1.1.5/EventMachine/HttpClient
      # puts http.response
      # p http.response_header

      @_completed += 1
      @ended_at = Time.now.to_i

      print_msg
      stop timer if @max_count - @_completed - @_filed < 1
    }

    request.errback { |http|
      @_filed += 1
      @_errors << http

      print_msg

      @ended_at = Time.now.to_i

      stop timer if @max_count - @_completed - @_filed < 1
    }

    @until_the_end = @max_count - @_initiated

    unless @until_the_end > 0
      EM.cancel_timer timer

      @initiated_at = Time.now.to_i
    end
  }
}

puts "\r\n"

puts "Initiate time (sec): #{(@initiated_at - @started_at).to_i}"

File.open('errors', File::RDWR|File::CREAT, 0644)  do |f|
  f.truncate 0
  unless @_errors.empty?
    errors_strings = @_errors.map { |http|
      "error: #{http.error}\r\nresponse: #{http.response.inspect}"
    }
    f.write errors_strings.join "\r\n\r\n"
  end
end
puts "Errors: #{@_errors.size}" if @_errors.empty?
